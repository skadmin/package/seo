<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200913043110 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE seo (id INT AUTO_INCREMENT NOT NULL, is_default TINYINT(1) DEFAULT \'0\' NOT NULL, entity VARCHAR(255) DEFAULT NULL, entity_id VARCHAR(255) DEFAULT NULL, url VARCHAR(255) DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, robots VARCHAR(255) DEFAULT NULL, meta_author VARCHAR(255) DEFAULT NULL, meta_description VARCHAR(255) DEFAULT NULL, meta_keywords VARCHAR(255) DEFAULT NULL, og_title VARCHAR(255) DEFAULT NULL, og_description VARCHAR(255) DEFAULT NULL, og_image VARCHAR(255) DEFAULT NULL, og_url VARCHAR(255) DEFAULT NULL, og_site_name VARCHAR(255) DEFAULT NULL, twitter_title VARCHAR(255) DEFAULT NULL, twitter_description VARCHAR(255) DEFAULT NULL, twitter_image VARCHAR(255) DEFAULT NULL, twitter_image_alt VARCHAR(255) DEFAULT NULL, twitter_card VARCHAR(255) DEFAULT NULL, twitter_site VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');

        // DATA
        $data = [
            'is_default' => 1,
            'robots'     => 'noindex, nofollow',
        ];

        $this->addSql('INSERT INTO seo (is_default, robots) VALUES (:is_default, :robots)', $data);
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE seo');
    }
}
