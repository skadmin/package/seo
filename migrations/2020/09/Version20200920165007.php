<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200920165007 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'seo.overview', 'hash' => '3f1db575e1779a4aa16c9ba3d7375e25', 'module' => 'admin', 'language_id' => 1, 'singular' => 'SEO', 'plural1' => '', 'plural2' => ''],
            ['original' => 'seo.overview.title', 'hash' => '10338851cd4f5fc7732202b2cf4d18d1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'SEO|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.seo.overview.name', 'hash' => '3f3495060d10ab728af1143fda7b9a47', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.seo.overview.action.edit', 'hash' => '32fc817b43e4f82614086092120313ea', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'seo.edit.title - %s', 'hash' => '1064f2951187eba3c063f54f5360080d', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace SEO', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.title.base', 'hash' => '0a9bbb4b01656962c7b98adb45622c85', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Základní nastavení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.title', 'hash' => 'a57ad64c72162fb6fd29d2ac404b047c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nadpis (title)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.robots', 'hash' => '69dabb535f032432c53e931c9c336b8e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Robots (robots)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.title.meta', 'hash' => '93de900a620172d19a63b92c0ebee61f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Meta nastavení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.meta-author', 'hash' => 'd52439af7c6079da9b1108f5e0e7709c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Autor (author)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.meta-keywords', 'hash' => '006d891dde1946316cea95920930f2d9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Klíčová slova (keywords)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.meta-description', 'hash' => 'd1b51eb55a70e923e40cd7a0d9516be7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis (description)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.title.facebook', 'hash' => 'd05fd540a5a09065fb9d48976d3133c1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Facebook nastavení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.og-title', 'hash' => '053919203ab5efdb60d2c521a6b898b9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nadpis (og:title)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.og-image', 'hash' => '24a52a5e45327ae29540ea74021d1a58', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obrázek (og:image)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.og-site_name', 'hash' => '0f01e92dc4b3169c9dcb6411dbd573a0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název sítě (og:site_name)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.og-url', 'hash' => 'ed8317a705f5621e725952b008679c4a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'URL (og:url)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.og-description', 'hash' => 'e2436b36b65a3d9ff13dd51fc2cfd9fb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis (og:description)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.title.twitter', 'hash' => '67badd023f7effe7ed32d94f790fc9e7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Twitter nastavení', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.twitter-title', 'hash' => '875cd2419fe341cf0fa93503907f1048', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nadpis (twitter:title)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.twitter-card', 'hash' => 'c21ce79a56f9e330bb182610e745cd07', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Karta (twitter:card)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.twitter-site', 'hash' => '3cd574bff59b10a4138a96484759f585', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název sítě (twitter:site)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.twitter-image', 'hash' => 'c4aeacba6be521c62e6513d2983d7ea1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obrázek (twitter:image)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.twitter-image-alt', 'hash' => '12072ab7a2d19af7a9d95ee160f18d04', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis obrázku (twitter:image:alt)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.twitter-description', 'hash' => 'ef770673b7479261947f07445b185a47', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis (twitter:description)', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.send', 'hash' => '11616c2c344802d5f4caa4db0207053b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.send-back', 'hash' => '776a04a3875cd03317a1503d3c705d85', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.back', 'hash' => '4c03c7a8c296ac8fed3a327bb7923225', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.flash.success.update', 'hash' => '9fcad6cf3518519e020fae1d123318ba', 'module' => 'admin', 'language_id' => 1, 'singular' => 'SEO bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'seo.edit.title', 'hash' => 'ea445f013d4a58ef693f1e7f402b4f3a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení SEO', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
