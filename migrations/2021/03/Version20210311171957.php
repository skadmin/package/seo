<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210311171957 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'edit.tab.general', 'hash' => 'ef734c8140cfcccb3b6ec1ff2a4d7d05', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Základní údaje', 'plural1' => '', 'plural2' => ''],
            ['original' => 'edit.tab.seo', 'hash' => '8f5a04efe904bcf1b7690ce9fa946eec', 'module' => 'admin', 'language_id' => 1, 'singular' => 'SEO', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.title-separator', 'hash' => '553515e8c246cbf578e97f206748d05f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Oddělovač', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.title-separator.req', 'hash' => '72b02fea74056358d394246ed7c28a6a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím oddělovač', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.title-website', 'hash' => '7260dabaa6c9da363df962698124df3c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název stránek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.seo.edit.title-website.req', 'hash' => '8b528f909d83ea8cde1230a8c36baef1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název stránek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.seo.title', 'hash' => '7d09433c4b0b1013aaf2f4f550df33c8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'SEO', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.seo.description', 'hash' => '175028742abc25a6eb5ae1bb336b15d7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat SEO stránek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'edit.tab.seo.deactive', 'hash' => 'df458c2678777c877fae25b2a31c9888', 'module' => 'admin', 'language_id' => 1, 'singular' => 'SEO lze nastavit po uložení', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
