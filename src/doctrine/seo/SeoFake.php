<?php

declare(strict_types=1);

namespace Skadmin\Seo\Doctrine\Seo;

final class SeoFake
{
    private string  $id;
    private ?string $name = null;

    public function __construct(string $id, ?string $name = null)
    {
        $this->id   = $id;
        $this->name = $name;
    }

    public static function create(string $id, ?string $name = null): self
    {
        return new SeoFake($id, $name);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }
}
