<?php

declare(strict_types=1);

namespace Skadmin\Seo\Doctrine\Seo;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;
use SkadminUtils\DoctrineTraits\Entity;

use function explode;
use function implode;
use function sprintf;
use function str_replace;
use function trim;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Seo
{
    use Entity\Id;

    #[ORM\Column(options: ['default' => false])]
    private bool $isDefault = false;

    #[ORM\Column(nullable: true)]
    protected ?string $entity = null;

    #[ORM\Column(nullable: true)]
    protected ?string $entityId = null;

    #[ORM\Column]
    private string $entityName = '';

    #[ORM\Column(nullable: true)]
    protected ?string $url = null;

    // SEO

    #[ORM\Column(nullable: true)]
    protected ?string $title = null;

    protected ?string $titleSeparator = null;

    protected ?string $titleWebsite = null;

    #[ORM\Column(nullable: true)]
    protected ?string $robots = null;

    // META

    #[ORM\Column(nullable: true)]
    protected ?string $metaAuthor = null;

    #[ORM\Column(nullable: true)]
    protected ?string $metaDescription = null;

    #[ORM\Column(nullable: true)]
    protected ?string $metaKeywords = null;

    // OG

    #[ORM\Column(nullable: true)]
    protected ?string $ogTitle = null;

    #[ORM\Column(nullable: true)]
    protected ?string $ogDescription = null;

    #[ORM\Column(nullable: true)]
    protected ?string $ogImage = null;

    #[ORM\Column(nullable: true)]
    protected ?string $ogUrl = null;

    #[ORM\Column(nullable: true)]
    protected ?string $ogSiteName = null;

    // Twitter

    #[ORM\Column(nullable: true)]
    protected ?string $twitterTitle = null;

    #[ORM\Column(nullable: true)]
    protected ?string $twitterDescription = null;

    #[ORM\Column(nullable: true)]
    protected ?string $twitterImage = null;

    #[ORM\Column(nullable: true)]
    protected ?string $twitterImageAlt = null;

    #[ORM\Column(nullable: true)]
    protected ?string $twitterCard = null;

    #[ORM\Column(nullable: true)]
    protected ?string $twitterSite = null;

    public function createByEntity(string $entity, string $entityId): void
    {
        $this->entity   = $entity;
        $this->entityId = $entityId;
    }

    public function createByUrl(string $url): void
    {
        $this->url = $url;
    }

    public function update(?string $entityName, ?string $title, ?string $titleSeparator, ?string $titleWebsite, ?string $robots, ?string $metaAuthor, ?string $metaDescription, ?string $metaKeywords, ?string $ogTitle, ?string $ogDescription, ?string $ogImage, ?string $ogUrl, ?string $ogSiteName, ?string $twitterTitle, ?string $twitterDescription, ?string $twitterImage, ?string $twitterImageAlt, ?string $twitterCard, ?string $twitterSite): void
    {
        if ($entityName !== null && trim($entityName) !== '') {
            $this->entityName = $entityName;
        }

        $this->title              = $title;
        $this->titleSeparator     = $titleSeparator;
        $this->titleWebsite       = $titleWebsite;
        $this->robots             = $robots;
        $this->metaAuthor         = $metaAuthor;
        $this->metaDescription    = $metaDescription;
        $this->metaKeywords       = $metaKeywords;
        $this->ogTitle            = $ogTitle;
        $this->ogDescription      = $ogDescription;
        $this->ogUrl              = $ogUrl;
        $this->ogSiteName         = $ogSiteName;
        $this->twitterTitle       = $twitterTitle;
        $this->twitterDescription = $twitterDescription;
        $this->twitterImageAlt    = $twitterImageAlt;
        $this->twitterCard        = $twitterCard;
        $this->twitterSite        = $twitterSite;

        if ($ogImage !== null) {
            $this->ogImage = $ogImage;
        }

        if ($twitterImage === null) {
            return;
        }

        $this->twitterImage = $twitterImage;
    }

    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function getEntityId(): ?string
    {
        return $this->entityId;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getEntityName(): string
    {
        return $this->entityName;
    }

    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    public function getName(): string
    {
        if ($this->isDefault()) {
            $name = 'Výchozí / Default';
        } elseif ($this->getUrl() !== null) {
            $name = $this->getUrl();
        } else {
            $name = sprintf('%s:%s', $this->getEntity(), $this->getEntityId());
        }

        if (trim($this->getEntityName()) !== '') {
            $name = sprintf('%s|%s', Strings::firstUpper(Strings::lower(str_replace('|', ' - ', $this->getEntityName()))), $name);
        }

        return $name;
    }

    /**
     * SEO
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getRobots(): ?string
    {
        return $this->robots;
    }

    /**
     * META
     */
    public function getMetaAuthor(): ?string
    {
        return $this->metaAuthor;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * OG
     */
    public function getOgTitle(): ?string
    {
        return $this->ogTitle;
    }

    public function getOgDescription(bool $force = false): ?string
    {
        if ($force || $this->ogDescription !== '') {
            return $this->ogDescription;
        }

        return $this->getMetaDescription();
    }

    public function getOgImage(): ?string
    {
        return $this->ogImage;
    }

    public function getOgUrl(): ?string
    {
        return $this->ogUrl;
    }

    public function getOgSiteName(): ?string
    {
        return $this->ogSiteName;
    }

    /**
     * TWITTER
     */
    public function getTwitterTitle(): ?string
    {
        return $this->twitterTitle;
    }

    public function getTwitterDescription(bool $force = false): ?string
    {
        if ($force || $this->twitterDescription !== '') {
            return $this->twitterDescription;
        }

        return $this->getMetaDescription();
    }

    public function getTwitterImage(): ?string
    {
        return $this->twitterImage;
    }

    public function getTwitterImageAlt(): ?string
    {
        return $this->twitterImageAlt;
    }

    public function getTwitterCard(): ?string
    {
        return $this->twitterCard;
    }

    public function getTwitterSite(): ?string
    {
        return $this->twitterSite;
    }

    /**
     * WEBSITE
     */
    public function getTitleSeparator(): ?string
    {
        return $this->titleSeparator;
    }

    public function getTitleWebsite(): ?string
    {
        return $this->titleWebsite;
    }

    #[ORM\PostLoad]
    public function onPostLoad(): void
    {
        @[$title, $titleSeparator, $titleWebsite] = explode(':-:-:', (string) $this->title);

        $this->title          = $title;
        $this->titleSeparator = $titleSeparator;
        $this->titleWebsite   = $titleWebsite;
    }

    #[ORM\PreUpdate]
    #[ORM\PrePersist]
    public function onPreUpdatePersist(): void
    {
        $this->title = implode(':-:-:', [$this->title, $this->titleSeparator, $this->titleWebsite]);
    }
}
