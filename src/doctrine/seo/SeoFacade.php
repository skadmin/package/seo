<?php

declare(strict_types=1);

namespace Skadmin\Seo\Doctrine\Seo;

use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;

use function is_bool;
use function is_object;
use function method_exists;
use function trim;

final class SeoFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Seo::class;
    }

    public function createByEntity(string $entity, string $entityId): Seo
    {
        $seo = $this->get();

        $seo->createByEntity($entity, $entityId);

        $this->em->persist($seo);
        $this->em->flush();

        return $seo;
    }

    public function createByUrl(string $url): Seo
    {
        $seo = $this->get();

        $seo->createByUrl($url);

        $this->em->persist($seo);
        $this->em->flush();

        return $seo;
    }

    public function update(?int $id, ?string $entityName, ?string $title, ?string $titleSeparator, ?string $titleWebsite, ?string $robots, ?string $metaAuthor, ?string $metaDescription, ?string $metaKeywords, ?string $ogTitle, ?string $ogDescription, ?string $ogImage, string $ogUrl, ?string $ogSiteName, ?string $twitterTitle, ?string $twitterDescription, ?string $twitterImage, ?string $twitterImageAlt, ?string $twitterCard, ?string $twitterSite): Seo
    {
        $seo = $this->get($id);

        $seo->update(
            $entityName,
            $title,
            $titleSeparator,
            $titleWebsite,
            $robots,
            $metaAuthor,
            $metaDescription,
            $metaKeywords,
            $ogTitle,
            $ogDescription,
            $ogImage,
            $ogUrl,
            $ogSiteName,
            $twitterTitle,
            $twitterDescription,
            $twitterImage,
            $twitterImageAlt,
            $twitterCard,
            $twitterSite
        );

        $this->em->persist($seo);
        $this->em->flush();

        return $seo;
    }

    public function get(?int $id = null): Seo
    {
        if ($id === null) {
            return new Seo();
        }

        $seo = parent::get($id);

        if ($seo === null) {
            return new Seo();
        }

        return $seo;
    }

    public function findByEntity(string $entity, string|int $entityId): Seo
    {
        $criteria = [
            'entity'   => $entity,
            'entityId' => $entityId,
        ];

        $seo = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $seo ?? $this->get();
    }

    public function findByObject(mixed $object): ?Seo
    {
        if ($object === null || ! method_exists($object, 'getId')) {
            return null;
        }

        if (! is_object($object)) {
            return null;
        }

        $className = $object::class;
        if (is_bool($className) && trim($className) === '') { //@phpstan-ignore-line
            return null;
        }

        $seo = $this->findByEntity($className, $object->getId());

        return $seo->isLoaded() ? $seo : null;
    }

    public function getDefault(): ?Seo
    {
        $criteria = ['isDefault' => true];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
