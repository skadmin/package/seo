<?php

declare(strict_types=1);

namespace Skadmin\Seo\Utils;

use Nette\Application\LinkGenerator;
use Nette\Utils\Html;
use Skadmin\Seo\BaseControl;
use Skadmin\Seo\Doctrine\Seo\SeoFake;
use Skadmin\Translator\Translator;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Toolbar\ToolbarButton;

class UtilsSeoRenderSeoHref
{
    private static Translator    $translator;
    private static LinkGenerator $linkGenerator;

    public function __construct(Translator $translator, LinkGenerator $linkGenerator)
    {
        self::$translator    = $translator;
        self::$linkGenerator = $linkGenerator;
    }

    public static function getHtmlSeoTab(mixed $objectSeo): Html
    {
        $li   = Html::el('li', ['class' => 'nav-item']);
        $icon = Html::el('i', ['class' => 'fab fa-fw fa-medapps']);
        $span = Html::el('span', ['class' => 'd-none d-lg-inline-block'])
            ->setText(self::$translator->translate('edit.tab.seo'));

        if ($objectSeo->isLoaded()) {
            $href = self::$linkGenerator->link('Admin:Component:default', [
                'package'    => (new BaseControl()),
                'render'     => 'edit',
                'isModal'    => 1,
                'entity'     => $objectSeo::class,
                'entityId'   => $objectSeo->getId(),
                'entityName' => $objectSeo->getName() ?? null,
            ]);

            $innerHtml = Html::el('a', [
                'class' => 'nav-link ajax',
                'href'  => $href,
            ]);
        } else {
            $li->addAttributes([
                'title'       => self::$translator->translate('edit.tab.seo.deactive'),
                'data-toggle' => 'tooltip',
            ]);

            $innerHtml = Html::el('span', ['class' => 'nav-link disabled']);
        }

        $innerHtml->setHtml($icon)
            ->addHtml($span);
        $li->setHtml($innerHtml);

        return $li;

        //{if $article->isLoaded()}
        //    <li class="nav-item">
        //        <a class="nav-link ajax"
        //           href="{plink :Admin:Component:default package => (new Skadmin\Seo\BaseControl), render => 'edit', isModal => 1, entity => get_class($article), entityId => $article->getId(), entityName => $article->getName()}">
        //            <i class="fab fa-fw fa-medapps"></i> <span class="d-none d-lg-inline-block">{_'edit.tab.seo'}</span>
        //        </a>
        //    </li>
        //{else}
        //    <li class="nav-item"
        //        title="{_'edit.tab.seo.deactive'}"
        //        data-toggle="tooltip">
        //        <span class="nav-link disabled">
        //            <i class="fab fa-fw fa-medapps"></i> <span class="d-none d-lg-inline-block">{_'edit.tab.seo'}</span>
        //        </span>
        //    </li>
        //{/if}
    }

    public static function createDataGridSeoToolbar(DataGrid $grid, string $type, ?string $name): ToolbarButton
    {
        $fakeSeo = new SeoFake($type, $name);

        $href   = ':Admin:Component:default';
        $params = [
            'package'    => (new BaseControl()),
            'render'     => 'edit',
            'isModal'    => 1,
            'entity'     => $fakeSeo::class,
            'entityId'   => $fakeSeo->getId(),
            'entityName' => $fakeSeo->getName(),
        ];

        $toolbar = new ToolbarButton($grid, $href, 'edit.tab.seo', $params);
        //$toolbar->setIcon('fw medapps');
        $toolbar->setClass('ajax btn btn-xs btn-outline-primary');

        return $toolbar;
    }
}
