<?php

declare(strict_types=1);

namespace Skadmin\Seo\Utils;

use Exception;
use Nette\Http\Request;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Skadmin\Seo\Doctrine\Seo\Seo;
use Skadmin\Seo\Doctrine\Seo\SeoFacade;
use SkadminUtils\ImageStorage\ImageStorage;

use function array_filter;
use function array_merge;
use function in_array;
use function is_string;
use function sprintf;
use function trim;

class UtilsSeo
{
    private const SEO_IMAGE_SIZE = [
        'og:image'      => '1200x627',
        'twitter:image' => '1200x600',
    ];

    private Request      $request;
    private SeoFacade    $facadeSeo;
    private ImageStorage $imageStorage;
    private Seo          $defaultSeo;

    /** @var string[] */
    private array $prepareSeoData = [];

    public function __construct(Request $request, SeoFacade $facadeSeo, ImageStorage $imageStorage)
    {
        $this->request      = $request;
        $this->facadeSeo    = $facadeSeo;
        $this->imageStorage = $imageStorage;

        $defaultSeo = $this->facadeSeo->getDefault();

        if ($defaultSeo === null) {
            throw new Exception('Default seo not found!');
        }

        $this->defaultSeo = $defaultSeo;
    }

    /**
     * @return string|string[]|null
     */
    public function getSeoAttr(?string $key = null, ?string $default = null): string|array|null
    {
        if ($key === null) {
            return $this->prepareSeoData;
        }

        return $this->prepareSeoData[$key] ?? $default;
    }

    public function getHtmlSeo(mixed $objectSeo): Html
    {
        $objectSeo = $this->facadeSeo->findByObject($objectSeo);

        return $this->prepareSeoData($this->defaultSeo, $objectSeo);
    }

    private function prepareSeoData(?Seo $default, ?Seo $seo): Html
    {
        $arraySeo             = array_merge($this->prepareSeoArray($default, false), $this->prepareSeoArray($seo));
        $this->prepareSeoData = $arraySeo;

        $arraySeo = array_filter($arraySeo, static function ($value): bool {
            return is_string($value) && trim($value) !== ''; //@phpstan-ignore-line
        });

        return $this->generateHtmlSeo($arraySeo);
    }

    /**
     * @return array<string>
     */
    private function prepareSeoArray(?Seo $seo, bool $onlyValue = true): array
    {
        if ($seo === null) {
            return [];
        }

        $seoData = [
            'titleSeparator'      => $seo->getTitleSeparator(),
            'titleWebsite'        => $seo->getTitleWebsite(),
            'robots'              => $seo->getRobots(),
            'author'              => $seo->getMetaAuthor(),
            'description'         => $seo->getMetaDescription(),
            'keywords'            => $seo->getMetaKeywords(),
            // og
            'og:title'            => $seo->getOgTitle(),
            'og:description'      => $seo->getOgDescription(),
            'og:image'            => $seo->getOgImage(),
            'og:url'              => $seo->getOgUrl(),
            'og:site_name'        => $seo->getOgSiteName(),
            // twitter
            'twitter:title'       => $seo->getTwitterTitle(),
            'twitter:description' => $seo->getTwitterDescription(),
            'twitter:image'       => $seo->getTwitterImage(),
            'twitter:image:alt'   => $seo->getTwitterImageAlt(),
            'twitter:card'        => $seo->getTwitterCard(),
            'twitter:site'        => $seo->getTwitterSite(),
        ];

        if ($seo->isDefault()) {
            Arrays::insertBefore($seoData, 'titleSeparator', ['titleDefault' => $seo->getTitle()]);
        } else {
            Arrays::insertBefore($seoData, 'titleSeparator', ['title' => $seo->getTitle()]);
        }

        if (! $onlyValue) {
            return $seoData;
        }

        return array_filter($seoData, static function ($value): bool {
            return $value !== null && trim($value) !== '';
        });
    }

    /**
     * @param array<string> $arraySeo
     */
    private function generateHtmlSeo(array $arraySeo): Html
    {
        $html = new Html();

        foreach ($arraySeo as $key => $value) {
            if (Strings::startsWith($key, 'title')) {
                continue;
            }

            if (in_array($key, ['og:image', 'twitter:image'], true)) {
                $image = $this->imageStorage->fromIdentifier([$value, self::SEO_IMAGE_SIZE[$key], 'shrink_only', 90]);
                $path  = sprintf('%s%s/%s', $this->request->getUrl()->getBaseUrl(), $image->data_dir, $image->identifier);
                $value = $path;
            }

            $meta = Html::el('meta', [
                'name'    => $key,
                'content' => $value,
            ]);

            $html->addHtml($meta);
        }

        // base meta

//<meta name="author" n:if="$seo->getSeoMetaAuthor()" content="{$seo->getSeoMetaAuthor()}">
//<meta name="description" n:if="$seo->getSeoMetaDescription()" content="{$seo->getSeoMetaDescription()}">
//<meta name="keywords" n:if="$seo->getSeoMetaKeywords()" content="{$seo->getSeoMetaKeywords()}">
//<meta name="robots" n:if="$seo->getSeoRobots()" content="{$seo->getSeoRobots()}">

// open graph data
//<meta property="og:title" n:if="$seo->getSeoOgTitle()" content="{$seo->getSeoOgTitle()}">
//<meta property="og:description" n:if="$seo->getSeoOgDescription()" content="{$seo->getSeoOgDescription()}">
//<meta property="og:image" n:if="$seo->getSeoOgImage()" content="{link //Homepage:}storage/images/{$seo->getSeoOgImage()}">
//<meta property="og:url" n:if="$seo->getSeoOgUrl()" content="{$seo->getSeoOgUrl()}">
//<meta property="og:site_name" n:if="$seo->getSeoOgSiteName()" content="{$seo->getSeoOgSiteName()}">

// twitter data
//<meta name="twitter:title" n:if="$seo->getSeoTwitterTitle()" content="{$seo->getSeoTwitterTitle()}">
//<meta name="twitter:description" n:if="$seo->getSeoTwitterDescription()" content="{$seo->getSeoTwitterDescription()}">
//<meta name="twitter:image" n:if="$seo->getSeoTwitterImage()" content="{link //Homepage:}storage/images/{$seo->getSeoTwitterImage()}">
//<meta name="twitter:image:alt" n:if="$seo->getSeoTwitterImageAlt()" content="{$seo->getSeoTwitterImageAlt()}">
//<meta name="twitter:card" n:if="$seo->getSeoTwitterCard()" content="{$seo->getSeoTwitterCard()}">
//<meta name="twitter:site" n:if="$seo->getSeoTwitterSite()" content="{$seo->getSeoTwitterSite()}">

        return $html;
    }
}
