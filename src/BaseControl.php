<?php

declare(strict_types=1);

namespace Skadmin\Seo;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'seo';
    public const DIR_IMAGE = 'seo';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fab fa-fw fa-medapps']),
            'items'   => ['overview'],
        ]);
    }
}
