<?php

declare(strict_types=1);

namespace Skadmin\Seo\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
