<?php

declare(strict_types=1);

namespace Skadmin\Seo\Components\Admin;

use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Seo\BaseControl;
use Skadmin\Seo\Doctrine\Seo\Seo;
use Skadmin\Seo\Doctrine\Seo\SeoFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class Overview extends GridControl
{
    use APackageControl;

    private SeoFacade $facade;

    public function __construct(SeoFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'seo.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.isDefault', 'DESC')
            ->addOrderBy('a.url', 'ASC')
            ->addOrderBy('a.entity', 'ASC'));

        // COLUMNS
        $grid->addColumnText('name', 'grid.seo.overview.name')
            ->setRenderer(function (Seo $seo): Html {
                $link = $this->getPresenter()->link('Component:default', [
                    'package' => new BaseControl(),
                    'render'  => 'edit',
                    'id'      => $seo->getId(),
                ]);

                $href = Html::el('a', [
                    'href'  => $link,
                    'class' => 'font-weight-bold',
                ]);

                $href->addText($seo->getName());

                return $href;
            });

        // FILTER
        $grid->addFilterText('name', 'grid.seo.overview.name');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.seo.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-primary');
        }

        // TOOLBAR
        // ALLOW

        return $grid;
    }
}
