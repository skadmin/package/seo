<?php

declare(strict_types=1);

namespace Skadmin\Seo\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Exception;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Controls\SubmitButton;
use Nette\Http\Request;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Seo\BaseControl;
use Skadmin\Seo\Doctrine\Seo\Seo;
use Skadmin\Seo\Doctrine\Seo\SeoFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;
use function is_bool;
use function trim;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private SeoFacade     $facade;
    private Seo           $seo;
    private ImageStorage  $imageStorage;
    private Request       $httpRequest;
    private string        $entity     = '';
    private string        $entityId   = '';
    private string        $entityName = '';

    public function __construct(?int $id, SeoFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage, Request $httpRequest)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;
        $this->httpRequest  = $httpRequest;

        $httpRequestUrl = $this->httpRequest->getUrl();
        if (intval($httpRequestUrl->getQueryParameter('isModal')) === 1) {
            $this->isModal = true;
            $this->drawBox = false;

            $this->entity     = $httpRequestUrl->getQueryParameter('entity');
            $this->entityId   = $httpRequestUrl->getQueryParameter('entityId');
            $this->entityName = $httpRequestUrl->getQueryParameter('entityName');

            if ($this->entity === null || $this->entityId === null) {
                throw new Exception('Entity/id for seo is not set!');
            }

            $this->seo = $this->facade->findByEntity($this->entity, $this->entityId); //@phpstan-ignore-line
        } else {
            $this->seo = $this->facade->get($id);
        }
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->seo->isLoaded()) {
            return new SimpleTranslation('seo.edit.title - %s', $this->seo->getName());
        }

        return 'seo.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        if (! $this->seo->isLoaded() && $values->entity !== null && $values->entityId !== null) {
            $this->seo = $this->facade->findByEntity($values->entity, $values->entityId);
        }

        // IDENTIFIERS
        $identifierOgImage      = UtilsFormControl::getImagePreview($values->ogImage, BaseControl::DIR_IMAGE);
        $identifierTwitterImage = UtilsFormControl::getImagePreview($values->twitterImage, BaseControl::DIR_IMAGE);

        // Nový záznam?
        if (! $this->seo->isLoaded()) {
            $seo = $this->facade->createByEntity($values->entity, $values->entityId);
            $this->onFlashmessage('form.seo.edit.flash.success.create', Flash::SUCCESS);
        } else {
            $seo = $this->seo;
        }

        // @phpstan-ignore-next-line
        if ($identifierOgImage !== false && $identifierOgImage !== null && $seo->getOgImage() !== null) {
            $this->imageStorage->delete($seo->getOgImage());
        }

        // @phpstan-ignore-next-line
        if ($identifierTwitterImage !== false && $identifierTwitterImage !== null && $seo->getTwitterImage() !== null) {
            $this->imageStorage->delete($seo->getTwitterImage());
        }

        $seo = $this->facade->update(
            $seo->getId(),
            $values->entityName,
            $values->title,
            $values->titleSeparator,
            $values->titleWebsite,
            $values->robots,
            $values->metaAuthor,
            $values->metaDescription,
            $values->metaKeywords,
            $values->ogTitle,
            $values->ogDescription,
            $identifierOgImage,
            $values->ogUrl,
            $values->ogSiteName,
            $values->twitterTitle,
            $values->twitterDescription,
            $identifierTwitterImage,
            $values->twitterImageAlt,
            $values->twitterCard,
            $values->twitterSite
        );

        if ($this->seo->isLoaded()) {
            $this->onFlashmessage('form.seo.edit.flash.success.update', Flash::SUCCESS);
        }

        if ($values->entity !== null && $values->entityId !== null) {
            $this->processOnBack(true);
        } elseif ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        } else {
            $this->getPresenter()->redirect('Component:default', [
                'package' => new BaseControl(),
                'render' => 'edit',
                'id' => $seo->getId(),
            ]);
        }
    }

    public function processOnBack(mixed $closeModal = false): void
    {
        if (! $closeModal instanceof SubmitButton && (! is_bool($closeModal) || $closeModal)) {
            $this->getPresenter()->redrawControl('snipModalClose');

            return;
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'overview',
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile($this->getControlTemplate(__DIR__ . '/edit.latte'));

        $template->seo     = $this->seo;
        $template->drawBox = $this->drawBox;
        $template->isModal = $this->isModal;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        // DATA
        $dataSeo = [
            'all' => 'all',
            'index, follow' => 'index, follow',
            'noindex, follow' => 'noindex, follow',
            'index, nofollow' => 'index, nofollow',
            'noindex, nofollow' => 'noindex, nofollow',
        ];

        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addHidden('entity', $this->entity);
        $form->addHidden('entityId', $this->entityId);
        $form->addHidden('entityName', $this->entityName);

        $form->addText('title', 'form.seo.edit.title');
        $inputTitleSeparator = $form->addText('titleSeparator', 'form.seo.edit.title-separator');
        $inputTitleWebsite   = $form->addText('titleWebsite', 'form.seo.edit.title-website');
        $form->addSelect('robots', 'form.seo.edit.robots', $dataSeo)
            ->setPrompt(Constant::PROMTP)
            ->setTranslator(null);

        // Meta
        $form->addText('metaAuthor', 'form.seo.edit.meta-author');
        $form->addTextArea('metaDescription', 'form.seo.edit.meta-description', null, 3);
        $form->addText('metaKeywords', 'form.seo.edit.meta-keywords');

        // Facebook
        $form->addText('ogTitle', 'form.seo.edit.og-title');
        $form->addTextArea('ogDescription', 'form.seo.edit.og-description', null, 3);
        $form->addImageWithRFM('ogImage', 'form.seo.edit.og-image');
        $form->addText('ogUrl', 'form.seo.edit.og-url');
        $form->addText('ogSiteName', 'form.seo.edit.og-site_name');

        // Twitter
        $form->addText('twitterTitle', 'form.seo.edit.twitter-title');
        $form->addTextArea('twitterDescription', 'form.seo.edit.twitter-description', null, 3);
        $form->addImageWithRFM('twitterImage', 'form.seo.edit.twitter-image');
        $form->addText('twitterImageAlt', 'form.seo.edit.twitter-image-alt');
        $form->addText('twitterCard', 'form.seo.edit.twitter-card');
        $form->addText('twitterSite', 'form.seo.edit.twitter-site');

        // BUTTON
        $form->addSubmit('send', 'form.seo.edit.send');
        $form->addSubmit('sendBack', 'form.seo.edit.send-back');
        $form->addSubmit('back', 'form.seo.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        if ($this->seo->isLoaded() && $this->seo->isDefault()) {
            $inputTitleSeparator->setRequired('form.seo.edit.title-separator.req');
            $inputTitleWebsite->setRequired('form.seo.edit.title-website.req');
        }

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->seo->isLoaded()) {
            return [];
        }

        if ($this->entityName !== null && trim($this->entityName) !== '') {
            $entityName = $this->entityName;
        } else {
            $entityName = $this->seo->getEntityName();
        }

        return [
            'entityName' => $entityName,
            'title' => $this->seo->getTitle(),
            'titleSeparator' => $this->seo->getTitleSeparator(),
            'titleWebsite' => $this->seo->getTitleWebsite(),
            'robots' => $this->seo->getRobots(),
            'metaAuthor' => $this->seo->getMetaAuthor(),
            'metaDescription' => $this->seo->getMetaDescription(),
            'metaKeywords' => $this->seo->getMetaKeywords(),
            // OG
            'ogTitle' => $this->seo->getOgTitle(),
            'ogDescription' => $this->seo->getOgDescription(true),
            //'ogImage'            => ['identifier' => $this->seo->getOgImage()],
            'ogUrl' => $this->seo->getOgUrl(),
            'ogSiteName' => $this->seo->getOgSiteName(),
            // TWITTER
            'twitterTitle' => $this->seo->getTwitterTitle(),
            'twitterDescription' => $this->seo->getTwitterDescription(true),
            //'twitterImage'       => ['identifier' => $this->seo->getTwitterImage()],
            'twitterImageAlt' => $this->seo->getTwitterImageAlt(),
            'twitterCard' => $this->seo->getTwitterCard(),
            'twitterSite' => $this->seo->getTwitterSite(),
        ];
    }
}
